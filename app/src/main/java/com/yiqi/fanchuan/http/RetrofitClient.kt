package com.yiqi.fanchuan.http

import com.google.gson.GsonBuilder
import com.safframework.http.interceptor.LoggingInterceptor
import com.yiqi.fanchuan.BuildConfig
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

class RetrofitClient {

    val MAX_CORE_SIZE by lazy { Runtime.getRuntime().availableProcessors() + 1 }
    var EXECUTOR = ThreadPoolExecutor(
        MAX_CORE_SIZE, MAX_CORE_SIZE * 10, 1,
        TimeUnit.SECONDS, LinkedBlockingDeque<Runnable>()
    )


    var okHttpClient: OkHttpClient
    var retrofit: Retrofit
    val TIMEOUT: Long = 10

    private constructor() {

        okHttpClient = OkHttpClient.Builder()
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor())
//            .addInterceptor(headerInterceptor())
            .addInterceptor(RequestBodyInterceptor())
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .build()

        val gson = GsonBuilder()
            .registerTypeAdapterFactory(BaseBeanTypeAdapterFactory())
            .create()

        retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.URL_BASE)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.from(EXECUTOR)))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient).build()
    }

    companion object {
        private var instance: RetrofitClient? = null
            get() {
                if (field == null) {
                    field = RetrofitClient()
                }
                return field
            }

        @Synchronized
        fun get(): RetrofitClient {
            return instance!!
        }

        @Synchronized
        fun setNull(){
            instance = null
        }
    }

//    private fun headerInterceptor():Interceptor{
//        return object :Interceptor{
//            override fun intercept(chain: Interceptor.Chain): Response {
//                val newRequest = chain.request().newBuilder()
//                    .addHeader("Authorization", "Bearer ${SPUtils.getString(Constants.SP_TOKEN)}")
//                    .build()
//                return chain.proceed(newRequest)
//            }
//        }
//    }


    private fun loggingInterceptor(): LoggingInterceptor {
        return LoggingInterceptor.Builder()
            .loggable(true)
            .request()
            .requestTag("PartnerHttp")
            .response()
            .hideVerticalLine()
            .responseTag("PartnerHttp") //.hideVerticalLine()// 隐藏竖线边框
            .build()
    }
}
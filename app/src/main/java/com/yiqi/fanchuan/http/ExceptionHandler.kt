/*
package com.yiqi.fanchuan.http


import com.google.gson.JsonParseException
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.text.ParseException
import javax.net.ssl.SSLHandshakeException

*/
/**
 * Author: zyf
 * Date: 2021/12/22
 * Time: 17:35
 *//*

class ExceptionHandler {

    companion object {
        private const val UNAUTHORIZED = 401
        private const val FORBIDDEN = 403
        private const val NOT_FOUND = 404
        private const val BAD_REQUEST = 400
        private const val REQUEST_TIMEOUT = 408
        private const val INTERNAL_SERVER_ERROR = 500
        private const val BAD_GATEWAY = 502
        private const val SERVICE_UNAVAILABLE = 503
        private const val GATEWAY_TIMEOUT = 504
        fun handleException(exception: Throwable) {
            var errorMsg :String? = ""
            if (exception is HttpException) {
                when (exception.code()) {
                    UNAUTHORIZED,
                    FORBIDDEN,
                    NOT_FOUND,
                    BAD_REQUEST,
                    REQUEST_TIMEOUT,
                    GATEWAY_TIMEOUT,
                    INTERNAL_SERVER_ERROR,
                    BAD_GATEWAY,
                    SERVICE_UNAVAILABLE -> {
                        errorMsg = AppManager.instance?.topActivity?.getString(R.string.network_error)
                    }
                }
            } else if (exception is ApiException) {
                errorMsg = exception.errMsg
                val errorCode = exception.errCode
                // 根据 errorCode 处理服务端接口异常，如 token 登录失效
                handleServerException(errorCode)
            } else if ((exception is JsonParseException) or (exception is JSONException) or (exception is ParseException)) {
                errorMsg = AppManager.instance?.topActivity?.getString(R.string.parsing_error)
            } else if (exception is ConnectException) {
                errorMsg = AppManager.instance?.topActivity?.getString(R.string.internet_connection_failed)
            } else if (exception is SSLHandshakeException) {
                errorMsg = AppManager.instance?.topActivity?.getString(R.string.certificate_verification_failed)
            } else if (exception is ConnectTimeoutException) {
                errorMsg = AppManager.instance?.topActivity?.getString(R.string.network_connection_timed_out)
            } else if (exception is SocketTimeoutException) {
                errorMsg = AppManager.instance?.topActivity?.getString(R.string.connection_timed_out)
            } else {
                errorMsg = AppManager.instance?.topActivity?.getString(R.string.the_network_link_is_abnormal)
            }
            if (BuildConfig.DEBUG){ //只有debug才可以进行错误提示
                T.instance.l(errorMsg?:"")
            }
        }

        private fun handleServerException(errorCode: Int) {
            when (errorCode) {

            }
        }
    }


}*/

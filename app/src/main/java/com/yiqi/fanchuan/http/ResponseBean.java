package com.yiqi.fanchuan.http;

import com.google.gson.annotations.SerializedName;

public class ResponseBean<T> {

    private static final int SUCCESS_CODE = 0;            // todo 成功
    private static final int SUCCESS_CODE_200 = 200;      // todo 成功
    private static final int MULTIDEVICE_CODE = 1001;     // todo 账号在其他设备登录
    private static final int TOKEN_EXPIRE_CODE = 1002;    // todo token 过期
    private static final int USER_IS_FENGJIN = 1003;      // todo 账号已被封禁
    private static final int ACCOUNT_FROZEN_CODE = 1004;  // todo 账号已被冻结


    @SerializedName(value = "status", alternate = {"code"})
    private int status;
    @SerializedName(value = "message", alternate = {"msg"})
    private String message;
    private T data;

    public boolean isSuccess() {
        return SUCCESS_CODE == status || SUCCESS_CODE_200 == status;
    }

    public boolean isMultipeDevice() {
        return MULTIDEVICE_CODE == status;
    }

    public boolean isAccountBan() {
        return USER_IS_FENGJIN == status;
    }

    public boolean isAccountFrozen() {
        return status == ACCOUNT_FROZEN_CODE;
    }

    public boolean isTokenExpire() {
        return status == TOKEN_EXPIRE_CODE;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseBean{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}

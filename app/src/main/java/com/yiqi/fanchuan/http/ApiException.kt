package com.yiqi.fanchuan.http

/**
 * Author: zyf
 * Date: 2021/12/22
 * Time: 17:34
 */
data class ApiException(var errCode: Int, var errMsg: String) : Exception()
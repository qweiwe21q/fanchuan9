package com.yiqi.fanchuan.http


import com.yiqi.fanchuan.base.IView
import com.yiqi.fanchuan.utils.AppManager
import com.yiqi.fanchuan.utils.log.L
import io.reactivex.observers.DisposableObserver
import retrofit2.HttpException


abstract class BaseObserver<T>: DisposableObserver<HttpResult<T>> {

    private var baseView: IView? = null
    private var isNeedShowDialog = false
    constructor() : super()

    constructor(view: IView?,isNeedShowDialog:Boolean = false) : super() {
        baseView = view
        this.isNeedShowDialog = isNeedShowDialog
    }

    override fun onStart() {
        super.onStart()
        if (isNeedShowDialog){
            baseView?.showLoading()
        }
    }

    override fun onNext(response: HttpResult<T>) {
        val code = response.code
        val errorMsg: String = response.message?:""
        if (code == 0 || code == 200) {
            onSuccess(response.data)
        }else {
            onError(ApiException(code, errorMsg))
        }
    }

    override fun onError(e: Throwable) {
        if (isNeedShowDialog){
            baseView?.hintDialog()
        }
        L.d("zyf onError:${e.message}")
        baseView?.executeOnLoadDataError(null)

    }

    abstract fun onSuccess(data: T?)

    override fun onComplete() {
        if (isNeedShowDialog){
            baseView?.hintDialog()
        }
    }


}
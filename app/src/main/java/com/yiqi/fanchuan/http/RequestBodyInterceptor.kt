package com.yiqi.fanchuan.http


import okhttp3.*


class RequestBodyInterceptor : Interceptor {

    private val PARAME_USERID = "userId"
    private val PARAME_ROLEID = "roleId"

    override fun intercept(chain: Interceptor.Chain): Response {
        val paramMap = mutableMapOf<String, String>()
        paramMap.put(PARAME_ROLEID,"")
        paramMap.put(PARAME_USERID, "")
        return chain.proceed(buildGetRequest(chain.request(),paramMap))
    }

    private fun buildGetRequest(request: Request, paramMap: MutableMap<String, String>): Request {
        val url = request.url

        val newUrl = url.newBuilder().run {
            for (param in paramMap) {
                removeAllQueryParameters(param.key)
                addQueryParameter(param.key,param.value)
            }
            build()
        }
        return request.newBuilder().url(newUrl).build()
    }

}
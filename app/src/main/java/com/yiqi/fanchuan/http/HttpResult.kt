package com.yiqi.fanchuan.http

/**
 * Author: zyf
 * Date: 2021/12/22
 * Time: 17:33
 */
data class HttpResult<T>(
        var data:T,
        var code :Int,
        var message:String?
)
package com.yiqi.fanchuan.http

import com.yiqi.fanchuan.bean.UserInfoBean
import com.yiqi.fanchuan.bean.WeatherBean
import io.reactivex.Observable
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.QueryMap


interface ApiService{

    /** 选手数据上报 **/
    @FormUrlEncoded
    @POST("racerinfo")
    fun reportinfo(@FieldMap params: Map<String, String>): Observable<HttpResult<String>>

    /** 登录 **/
    @GET("login")
    fun login(@QueryMap params: Map<String, String>): Observable<HttpResult<UserInfoBean>>

    /** 查看天气情况 **/
    @GET("weather")
    fun weather(): Observable<HttpResult<WeatherBean>>

    /** 接触浮标数据上传 **/
    @GET("pointcontact")
    fun pointcontact(@QueryMap params: Map<String, String>): Observable<HttpResult<String>>

}
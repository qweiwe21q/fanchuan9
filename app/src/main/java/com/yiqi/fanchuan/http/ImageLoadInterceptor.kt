package com.yiqi.fanchuan.http

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response

class ImageLoadInterceptor : Interceptor {

    private val TAG = "ImageLoadInterceptor"

    override fun intercept(chain: Interceptor.Chain): Response {

        val startTime = System.currentTimeMillis()
        val request = chain.request()
        val url = request.url
        val response = chain.proceed(request)
        val endTime = System.currentTimeMillis()
        val code = response.code

        Log.i(TAG,"${code} ${url} 耗时:(${endTime - startTime}) 大小:${response.body?.contentLength()}")

        return response
    }

}
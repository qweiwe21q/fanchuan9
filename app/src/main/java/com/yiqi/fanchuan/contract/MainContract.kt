package com.yiqi.fanchuan.contract

import com.yiqi.fanchuan.base.IView

/**
 * @Author zyf
 * @Date 2023/7/7 13:51
 * @Describe
 */

interface MainContract {

    interface View:IView{

    }

    interface Presenter{

    }
}
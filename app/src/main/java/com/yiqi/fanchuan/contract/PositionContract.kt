package com.yiqi.fanchuan.contract

import com.yiqi.fanchuan.base.IView
import com.yiqi.fanchuan.bean.WeatherBean

/**
 * @Author zyf
 * @Date 2023/7/10 17:47
 * @Describe
 */

interface PositionContract {

    interface View: IView {
        fun returnWeather(data: WeatherBean?)
    }

    interface Presenter{
        fun weather()
    }
}
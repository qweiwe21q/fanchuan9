package com.yiqi.fanchuan.contract

import com.yiqi.fanchuan.base.IView

/**
 * @Author zyf
 * @Date 2023/7/10 17:41
 * @Describe
 */

interface TrainContract {

    interface View: IView {

    }

    interface Presenter{
        fun reportinfo(map:HashMap<String,String>)
    }
}
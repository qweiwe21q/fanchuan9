package com.yiqi.fanchuan.contract

import com.yiqi.fanchuan.base.IView
import com.yiqi.fanchuan.bean.UserInfoBean

/**
 * @Author zyf
 * @Date 2023/8/31 15:09
 * @Describe
 */

interface LoginContract {

    interface View: IView {
        fun returnLogin(bean: UserInfoBean?)
    }

    interface Presenter{
        fun login(params: Map<String, String>)
    }
}
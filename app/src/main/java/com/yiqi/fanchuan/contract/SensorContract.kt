package com.yiqi.fanchuan.contract

import com.yiqi.fanchuan.base.IView

/**
 * @Author zyf
 * @Date 2023/7/10 18:20
 * @Describe
 */

interface SensorContract {

    interface View: IView {

    }

    interface Presenter{

    }
}
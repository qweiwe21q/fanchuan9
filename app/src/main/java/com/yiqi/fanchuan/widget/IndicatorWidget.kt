package com.yiqi.fanchuan.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.yiqi.fanchuan.R
import com.yiqi.fanchuan.utils.AnimatorUtils.rotate
import com.yiqi.fanchuan.utils.loadImage

/**
 * @Author zyf
 * @Date 2023/10/13 16:30
 * @Describe 指示器
 */

class IndicatorWidget(context: Context, attributes: AttributeSet) : ConstraintLayout(context, attributes){

    private val ivIndicatorBg by lazy { findViewById<ImageView>(R.id.iv_indicator_bg) }

    private val ivPointer by lazy { findViewById<ImageView>(R.id.iv_pointer) }

    private val tvValue by lazy { findViewById<TextView>(R.id.tv_value) }

    private val tvType by lazy { findViewById<TextView>(R.id.tv_type) }

    init {
        View.inflate(getContext(), R.layout.widget_indicator, this)

        val typedArray = context.obtainStyledAttributes(attributes, R.styleable.IndicatorWidget)
        val pointer = typedArray.getResourceId(R.styleable.IndicatorWidget_pointer, 0)
        val pointerWidth = typedArray.getDimension(R.styleable.IndicatorWidget_pointer_width, 0f)
        val pointerHeight = typedArray.getDimension(R.styleable.IndicatorWidget_pointer_height, 0f)

        val layoutParams = ivPointer.layoutParams
        layoutParams.height = pointerHeight.toInt()
        layoutParams.width = pointerWidth.toInt()
        ivPointer.layoutParams = layoutParams
        val dics = typedArray.getResourceId(R.styleable.IndicatorWidget_disc, 0)
        val type = typedArray.getString(R.styleable.IndicatorWidget_type)
        val textSize = typedArray.getDimension(R.styleable.IndicatorWidget_text_size,24f)

        ivIndicatorBg.loadImage(dics)
        tvType.setText(type)
        tvValue.textSize = textSize
        ivPointer.loadImage(pointer)
    }

    fun setDiscValue(value:Float){
        ivPointer.rotate(value)
    }

    fun setSpeed(speed:Int){
        tvValue.setText("$speed")
    }


}

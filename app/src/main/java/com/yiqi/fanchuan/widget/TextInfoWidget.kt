package com.yiqi.fanchuan.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import com.yiqi.fanchuan.R

/**
 * @Author zyf
 * @Date 2023/8/25 16:38
 * @Describe
 */

class TextInfoWidget(context: Context, attributes: AttributeSet) :LinearLayout(context, attributes){

    private val tvTitle by lazy { findViewById<TextView>(R.id.tv_title) }

    private val tvContent by lazy { findViewById<TextView>(R.id.tv_content) }

    init {
        initView()
        attributes.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.TextInfoWidget)
            val title = typedArray.getString(R.styleable.TextInfoWidget_title)
            tvTitle.setText(title)
            typedArray.recycle()
        }
    }

    fun initView(){
        val view = LayoutInflater.from(context).inflate(R.layout.text_info_widget, this)
    }

    fun setContent(content:String){
        tvContent.setText(content)
    }

    fun clean(){
        tvContent.setText("")
    }
}
package com.yiqi.fanchuan.widget

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.SweepGradient
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View
import com.yiqi.fanchuan.R


class AttendanceRateView(context: Context, attrs: AttributeSet? = null) :
    View(context, attrs) {

    private val mMinColors = intArrayOf(Color.parseColor("#6546D0"), Color.parseColor("#4CA2DF"),Color.parseColor("#6546D0"))
    var matrixStart = 270f //渐变旋转角度

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val rectF = RectF()
    private var percentage: Int = 0
    private val text = context.getString(R.string.circle_view_attendance)

    private val percentageTextColor: Int
    private val attendanceTextColor: Int
    private val circleBackgroundColor: Int
    private var remainder:String = ""

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.AttendanceRateView)
        percentageTextColor = typedArray.getColor(R.styleable.AttendanceRateView_percentageTextColor, Color.BLACK)
        attendanceTextColor = typedArray.getColor(R.styleable.AttendanceRateView_attendanceTextColor, Color.BLACK)
        circleBackgroundColor = typedArray.getColor(R.styleable.AttendanceRateView_circleBackgroundColor, resources.getColor(R.color.col_14192E))
        typedArray.recycle()

        paint.isAntiAlias = true
    }

    fun setPercentage(targetPercentage: Int) {
        this.percentage = targetPercentage
        invalidate()
    }

    /**
     * 设置倒计时
     * @param remainder
     */
    fun setRemainder(remainder:String){
        this.remainder = remainder
        invalidate()
    }

    fun animatePercentage(targetPercentage: Int, duration: Long = 1000) {
        val animator = ValueAnimator.ofInt(percentage, targetPercentage)
        animator.duration = duration
        animator.addUpdateListener { animation ->
            percentage = animation.animatedValue as Int
            invalidate()
        }
        animator.start()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val width = width
        val height = height
        val strokeWidth = 10
        val padding = 60

        // Draw the light blue background circle
        paint.color = circleBackgroundColor
        paint.strokeWidth = strokeWidth.toFloat()
        paint.style = Paint.Style.STROKE
        rectF.set(padding.toFloat(), padding.toFloat(), (width - padding).toFloat(), (height - padding).toFloat())
        canvas.drawOval(rectF, paint)

        // Draw the dark blue progress arc

        val mSweepGradient = SweepGradient(
            canvas.width / 2f,
            canvas.height / 2f,
            mMinColors, floatArrayOf(0f, 0.5f,1f)
        )
        val matrix = Matrix() //将渐变旋转

        matrix.setRotate(matrixStart, canvas.width / 2f, canvas.height / 2f)
        mSweepGradient.setLocalMatrix(matrix)
        //把渐变设置到笔刷
        paint.setShader(mSweepGradient)

        paint.strokeWidth = strokeWidth.toFloat() * 2.2f
        paint.strokeCap = Paint.Cap.ROUND // 设置笔尖样式为圆角
        val shadowColor = Color.argb(90, 49, 119, 248) // #503177F8
        paint.setShadowLayer(15f, 0f, 0f, shadowColor)
        canvas.drawArc(rectF, -90f, percentage * 3.6f, false, paint)
        paint.clearShadowLayer()
        paint.setShader(null)


        // Draw the percentage text
        paint.textSize = width / 7f
        paint.color = percentageTextColor
        paint.typeface = Typeface.DEFAULT
        paint.style = Paint.Style.FILL
        val percentageText = remainder
        val percentageTextWidth = paint.measureText(percentageText)
        canvas.drawText(percentageText, (width - percentageTextWidth) / 2, height / 2f, paint)

        // Draw the "出勤率" text
        paint.textSize = width / 13f
        paint.color = attendanceTextColor
        val textWidth = paint.measureText(text)
        canvas.drawText(text, (width - textWidth) / 2, height / 2f + width / 7f, paint)
    }
}

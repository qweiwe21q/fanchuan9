package com.yiqi.fanchuan.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.yiqi.fanchuan.R

class MediumBoldTextView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : AppCompatTextView(context, attrs, defStyleAttr) {

    private var width: Float = 1f

    init {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.MediumBoldTextView)
            width = typedArray.getFloat(R.styleable.MediumBoldTextView_stroke_width, width)
            typedArray.recycle()
        }
    }

    fun setWidth(width: Float) {
        this.width = width
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        val paint = paint
        paint.strokeWidth = width
        paint.style = Paint.Style.FILL_AND_STROKE
        super.onDraw(canvas)
    }
}
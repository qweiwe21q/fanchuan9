package com.yiqi.fanchuan.widget;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.yiqi.fanchuan.R;


public class CustomToast extends Toast {
    public CustomToast(Context context) {
        super(context);
    }
    public static CustomToast makeText(Context context, CharSequence text, int duration) {
        CustomToast customToast = new CustomToast(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_toast_layout, null);
        TextView textView = layout.findViewById(R.id.custom_toast_text);
        textView.setText(text);
        customToast.setView(layout);
        customToast.setDuration(duration);
        customToast.setGravity(Gravity.CENTER, 0, 0);
        return customToast;
    }
}
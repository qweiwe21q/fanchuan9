package com.yiqi.fanchuan.widget

import android.content.Context
import android.os.CountDownTimer
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.yiqi.fanchuan.R
import com.yiqi.fanchuan.utils.setOnClickListener
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

/**
 * @Author zyf
 * @Date 2023/9/27 13:59
 * @Describe
 */

class TitleWidget(context: Context, attributes: AttributeSet) : ConstraintLayout(context, attributes){

    private val tvTitle by lazy { findViewById<TextView>(R.id.tv_title_content) }

    private val tvTime by lazy {findViewById<TextView>(R.id.tv_now_time)}

    private val llBack by lazy { findViewById<LinearLayout>(R.id.ll_back) }

    private var title = ""

    private var countDownTimer :CountDownTimer? = null

    init {
        View.inflate(getContext(), R.layout.widget_title, this)


        val typedArray = context.obtainStyledAttributes(attributes, R.styleable.TitleWidget)
        title = typedArray.getString(R.styleable.TitleWidget_ttitle)?:""
        typedArray.recycle()

        initView()
        initClick()
        initTimer()

    }

    fun initView(){
        tvTitle.setText(title)
    }

    fun getBackView():View{
        return llBack
    }

    fun initClick(){

    }

    fun setTitle(title:String){
        tvTitle.setText(title)
    }


    fun initTimer(){
        //定时器 每秒更新航速、时间
        countDownTimer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val sdf = SimpleDateFormat("HH:mm", Locale.getDefault())
                val currentTime: String = sdf.format(Date())
                tvTime.setText(currentTime)
            }

            override fun onFinish() {
                start()
            }
        }
        countDownTimer?.start()
    }

    private var mCallBack : CallBack? = null

    fun setCallBack(callBack:CallBack){
        mCallBack = callBack
    }

    interface CallBack{
        fun back()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        countDownTimer?.cancel()
    }

}
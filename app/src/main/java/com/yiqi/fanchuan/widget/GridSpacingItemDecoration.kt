package com.yiqi.fanchuan.widget

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.yiqi.fanchuan.utils.SizeUtils

/**
 * 设置item的横纵向间距
 * @param spanCount 多少列
 * @param transverseSpacing 横向间距
 * @param directionSpacing 纵向间距
 * @param includeEdge 是否包括边缘
 *
 */
class GridSpacingItemDecoration(private val spanCount: Int, private val transverseSpacing: Int,private val directionSpacing: Int, private val includeEdge: Boolean) : RecyclerView.ItemDecoration() {
    val a = 0
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val mTransverseSpacing = SizeUtils.dip2px(view.context,transverseSpacing.toFloat())
        val mDirectionSpacing = SizeUtils.dip2px(view.context,directionSpacing.toFloat())

        val position = parent.getChildAdapterPosition(view) // item 的位置
        val column = position % spanCount // item 所在的列
        if (includeEdge) {
            outRect.left = mTransverseSpacing - column * mTransverseSpacing / spanCount
            outRect.right = (column + 1) * mTransverseSpacing / spanCount
            if (position < spanCount) {
                outRect.top = mDirectionSpacing
            }
            outRect.bottom = mDirectionSpacing
        } else {
            outRect.left = column * mTransverseSpacing / spanCount
            outRect.right = mTransverseSpacing - (column + 1) * mTransverseSpacing / spanCount
            if (position >= spanCount) {
                outRect.top = mDirectionSpacing
            }
        }
    }
}
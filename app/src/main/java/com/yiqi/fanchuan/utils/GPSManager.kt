package com.yiqi.fanchuan.utils

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.CountDownTimer
import com.yiqi.fanchuan.utils.log.truncateDecimal
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * @Author zyf
 * @Date 2023/8/30 17:24
 * @Describe  航速管理器
 */


class GPSManager private constructor(private val context: Context) {

    //位置管理器
    private val locationManager by lazy { context.getSystemService(Context.LOCATION_SERVICE) as LocationManager }

    val provider = LocationManager.GPS_PROVIDER

    private var reckonTime = 0L  //训练时间

    private var mSpeed = 0f  //速度

    private var speedOverGround = 0f  //航速

    companion object {
        @Volatile
        private var instance: GPSManager? = null

        fun getInstance(context: Context): GPSManager {
            return instance ?: synchronized(this) {
                instance ?: GPSManager(context).also { instance = it }
            }
        }
    }

    val locationRequest =  object : LocationListener {
        override fun onLocationChanged(location: Location) {
            //速度单位转换
            mSpeed = convertMetersPerSecondToNauticalMilesPerHour(location.speed)
            mGPSCallBack?.returnSpeed("$mSpeed")
            location.apply {
                mGPSCallBack?.returnLocation(latitude.truncateDecimal(4),longitude.truncateDecimal(4))
            }
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
        }
        override fun onProviderEnabled(provider: String) {
        }
        override fun onProviderDisabled(provider: String) {
        }
    }

    //定时器 每秒更新航速、时间
    val countDownTimer = object : CountDownTimer(60000, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            speedOverGround+=mSpeed
            reckonTime++
            mGPSCallBack?.returnTime("${formatTime(reckonTime)}")
            mGPSCallBack?.returnSpeedOverGround("${(speedOverGround/reckonTime).truncateDecimal(2)}")
        }

        override fun onFinish() {
            start()
        }
    }

    private lateinit var disposable: Disposable
    private var intervalTime: Long = 5000 // 初始间隔时间

    private fun startCountdownTimer() {
        val timerObservable = Observable.interval(0, intervalTime, TimeUnit.MILLISECONDS)
            .flatMap {
                // 执行倒计时逻辑
                Observable.just(it)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

        disposable = timerObservable.subscribe { time ->
            mGPSCallBack?.dataUpload()
            //电量大于百分之 20 5 秒上传一次 反之 30 秒上传一次
            if (getBatteryLevel(context) < 20 && intervalTime != 20000L){
                changeIntervalTime(20000)
            }else if (getBatteryLevel(context) > 20 && intervalTime != 5000L){
                changeIntervalTime(5000)
            }
        }
    }

    private fun changeIntervalTime(newIntervalTime: Long) {
        intervalTime = newIntervalTime
        // 修改间隔时间
        disposable.dispose()

        // 重新开始倒计时
        startCountdownTimer()
    }

    private fun stopCountdownTimer() {
        // 取消倒计时
        disposable.dispose()
    }

    /**
     * 停止比赛
     */
    fun stop(){
        reckonTime = 0
        speedOverGround = 0f
        mSpeed = 0f
        countDownTimer.cancel()
        stopCountdownTimer()
        locationManager.removeUpdates(locationRequest)
    }

    /**
     * 开始比赛
     */
    @SuppressLint("MissingPermission")
    fun start(){
        // 获取位置提供器
        locationManager.getProviders(true)
        // 请求位置更新   更新间隔时间，单位：毫秒    更新距离，单位：米
        locationManager.requestLocationUpdates(provider, 3000, 3f, locationRequest)
        locationManager.getLastKnownLocation(provider)?.apply {
            mGPSCallBack?.returnLocation(latitude.truncateDecimal(4),longitude.truncateDecimal(4))
        }
        countDownTimer.start()
        startCountdownTimer()
    }

    private var mGPSCallBack:GPSCallBack? = null

    interface GPSCallBack{
        //返回速度
        fun returnSpeed(speed:String)
        //返回时间
        fun returnTime(time:String)
        //返回航速
        fun returnSpeedOverGround(speedOverGround:String)
        //返回经纬度
        fun returnLocation(latitude:Double,longitude:Double)
        //数据上传
        fun dataUpload()
    }

    fun setGPSCallBack(GPSCallBack:GPSCallBack){
        mGPSCallBack = GPSCallBack
    }
}
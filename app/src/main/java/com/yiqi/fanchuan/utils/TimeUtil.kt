package com.yiqi.fanchuan.utils

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.concurrent.TimeUnit

/**
 * @Author zyf
 * @Date 2023/8/25 15:12
 * @Describe
 */

/**
 *  秒 转 时间
 *  @param seconds 秒
 */
fun formatTime(seconds: Long): String {
    val hours = seconds / 3600
    val minutes = (seconds % 3600) / 60
    val secs = seconds % 60

    val timeString = String.format("%02d:%02d:%02d", hours, minutes, secs)
    return timeString
}

/**
 *  时间戳 转 秒
 *  @param timestamp 时间戳
 */
fun timestampToSeconds(timestamp: Long): Long {
    return TimeUnit.MILLISECONDS.toSeconds(timestamp)
}

/**
 * 获取当前本机时间
 * @param pattern 时间格式
 */
fun getCurrentTime(pattern:String = "yyyy-MM-dd HH:mm:ss"): String {
    val currentTime = Date()
    val sdf = SimpleDateFormat(pattern)
    return sdf.format(currentTime)
}

/**
 * 计算距离比赛时间差值
 * @param startTime 比赛开始时间
 * @param timeLimit 限制时长 默认 10分钟
 */
fun getTimeDifference(startTime: String,timeLimit:Long = 600000): String? {
    if (startTime.isNullOrEmpty())return null
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

    val start = simpleDateFormat.parse(startTime)

    if (start == null) {
        return null
    }

    val end = Date() // 使用当前时间

    val difference = start.time-end.time

    if (difference < 0 || difference > 600000) {
        return null
    }

    val minutes = difference / 60000
    val seconds = (difference % 60000) / 1000

    return "$minutes:$seconds"

}


/**
 * 算出剩余时间的百分比
 */
fun getRemainingPercentage(startTime: String): Int? {

    if (startTime.isNullOrEmpty())return 0
    val minutes = startTime.split(":")[0].toInt()
    val seconds = startTime.split(":")[1].toInt()

    val totalSeconds = minutes * 60 + seconds

    if (totalSeconds <= 0 || totalSeconds > 600) {
        return null
    }

    val percentage = ((1 - totalSeconds.toDouble()/600)*100).toInt()

    return percentage

}

/**
 * 分秒转秒
 */
fun String.toTotalSeconds(): Long {
    val parts = split(":")
    val minutes = parts[0].toInt()
    val seconds = parts[1].toInt()

    return (minutes * 60 + seconds).toLong()
}

/**
 * 秒转分秒
 */
fun Long.secondsToTime(): String {
    val minutes = this / 60
    val seconds = this % 60

    return "${minutes}:${if(seconds < 10) "0" else ""}$seconds"
}
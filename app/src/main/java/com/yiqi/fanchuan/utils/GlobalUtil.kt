package com.yiqi.fanchuan.utils

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Intent
import android.content.IntentFilter
import android.content.res.AssetManager
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.os.BatteryManager
import android.os.Build
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.ScaleAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.yiqi.bianjiema.utils.GlideUtils
import com.yiqi.fanchuan.MyApp
import java.io.BufferedReader
import java.io.InputStreamReader
import java.math.BigDecimal
import java.math.BigInteger
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


/**
 * Author: zyf
 * Date: 2020-9-3
 * Time: 14:43
 */
/**
 * 获取资源文件中定义的字符串。
 *
 * @param resId
 * 字符串资源id
 * @return 字符串资源id对应的字符串内容。
 */
fun gString(resId: Int): String = MyApp.getContext().resources.getString(resId)

fun gColor(resId: Int): Int =  ContextCompat.getColor(MyApp.getContext(),resId)

fun gInteger(resId: Int): Int = MyApp.getContext().resources.getInteger(resId)

fun gDrawable(resId: Int): Drawable? = ContextCompat.getDrawable(MyApp.getContext(),resId)

/**
 * 除以函数，防止被除数为0
 * @param param 除数
 */
fun Int.divide(param:Int):Int{
    if (this <= 0 || param <= 0){
        return -1
    }
    return this/param
}

/**
 * 除以函数，防止被除数为0
 * @param param 除数
 */
fun Long.divide(param:Int):Long{
    if (this <= 0 || param <= 0){
        return -1
    }
    return this/param
}

/**
 * 除以函数，防止被除数为0
 * @param param 除数
 */
fun Long.divide(param:Long):Long{
    if (this <= 0 || param <= 0){
        return -1
    }
    return this/param
}

/**
 * 防止索引越界以及索引是-1的问题
 * @param index
 * @return T
 */
fun <T>List<T>.getValue(index: Int):T?{
    if (index < 0 || this.size <= index){
        return null
    }
    return get(index)
}

/**
 * 判断Activity是否Destroy
 * @param mActivity
 * @return
 */
fun isDestroy(mActivity: Activity?): Boolean {
    return if (mActivity == null || mActivity.isFinishing || Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && mActivity.isDestroyed) {
        true
    } else {
        false
    }
}

/**
 * 对目标Drawable 进行着色
 *
 * @param drawable 目标Drawable
 * @param color    着色的颜色值
 * @return 着色处理后的Drawable
 */
fun tintDrawable( drawable: Drawable, color: Int): Drawable {
    val arrayList = ArrayList<String>()
    val wrappedDrawable: Drawable = DrawableCompat.wrap(drawable)
    DrawableCompat.setTint(wrappedDrawable, color)
    return wrappedDrawable
}

/**
 * 判断一个字符是否是汉字
 * PS：中文汉字的编码范围：[\u4e00-\u9fa5]
 *
 * @param c 需要判断的字符
 * @return 是汉字(true), 不是汉字(false)
 */
 fun isChineseChar( c:String):Boolean {
    return c.matches(Regex("[\u4e00-\u9fa5]"));
}


/**
 * 判断一个字符是否是字母
 * PS：字母的编码范围：[^(A-Za-z)]
 *
 * @param c 需要判断的字符
 * @return 是字母(true), 不是字母(false)
 */
fun isLetter(c:String):Boolean {
    return c.matches(Regex("[(A-Za-z)]"));
}

/**
 * 校验名称
 * @param name 需要校验的名称
 * @return 合规(true), 不合规(false)
 */
fun nameIsCompliance(name:String):Boolean{
    name.forEach {
        if (!isChineseChar(it.toString()) && !isLetter(it.toString())){
            return false
        }
    }
    return true
}



/**
 * 截取文件名称
 * @param filePath 文件路径
 */
fun getFileName(filePath:String):String{
    val start: Int = filePath.lastIndexOf("/")
    return if (start != -1 && filePath.length > 1) {
        filePath.substring(start + 1, filePath.length)
    } else {
        ""
    }
}


var lastClickTime: Long = 0

/**
 *
 * 判断对于按钮快速点击时，前后时间相差0.5秒内时，识别为无效点击。
 *
 * @return
 */
fun isFastDoubleClick(): Boolean {
    synchronized(1) {
        val time = System.currentTimeMillis()
        val timeD = time - lastClickTime
        return if (timeD >= 0 && timeD <= 500) {
            true
        } else {
            lastClickTime = time
            false
        }
    }
}

/**
 * 批量设置控件点击事件。
 *
 * @param v 点击的控件
 * @param block 处理点击事件回调代码块
 */
fun setOnClickListener(vararg v: View?, block: View.() -> Unit) {
    val listener = View.OnClickListener { it.block() }
    v.forEach { it?.setOnClickListener(listener) }
}

/**
 * 根据其实颜色和渐变程度获取当前渐变色
 * @param fraction 当前分值 0~1
 * @param startColor 起始颜色
 * @param endColor 结束颜色
 */
fun getCurrentColor(fraction: Float, startColor: Int, endColor: Int): Int {
    val redCurrent: Int
    val blueCurrent: Int
    val greenCurrent: Int
    val alphaCurrent: Int

    val redStart = Color.red(startColor)
    val blueStart = Color.blue(startColor)
    val greenStart = Color.green(startColor)
    val alphaStart = Color.alpha(startColor)

    val redEnd = Color.red(endColor)
    val blueEnd = Color.blue(endColor)
    val greenEnd = Color.green(endColor)
    val alphaEnd = Color.alpha(endColor)

    val redDifference = redEnd - redStart
    val blueDifference = blueEnd - blueStart
    val greenDifference = greenEnd - greenStart
    val alphaDifference = alphaEnd - alphaStart

    redCurrent = (redStart + fraction * redDifference).toInt()
    blueCurrent = (blueStart + fraction * blueDifference).toInt()
    greenCurrent = (greenStart + fraction * greenDifference).toInt()
    alphaCurrent = (alphaStart + fraction * alphaDifference).toInt()

    return Color.argb(alphaCurrent, redCurrent, greenCurrent, blueCurrent)
}

/**
 * 代码实现圆角背景
 * @param radius      圆角
 * @param color       颜色
 * @param isFill      是否填充
 * @param strokeWidth 边框线宽度
 */
fun getRoundRectDrawable(radius: Int, color: Int, isFill: Boolean, strokeWidth: Int): GradientDrawable {
    //左上、右上、右下、左下的圆角半径
    val radius = floatArrayOf(radius.toFloat(), radius.toFloat(), radius.toFloat(), radius.toFloat(), 0f, 0f, 0f, 0f)
    val drawable = GradientDrawable()
    drawable.cornerRadii = radius
    drawable.setColor(color)
    drawable.setStroke(if (isFill) 0 else strokeWidth, color)
    return drawable
}


/**
 * String转float
 * @param str
 * @return
 */
fun toFloat(str: String?): Float {
    if (str == null) return 0F
    var value = 0f
    try {
        value = str.toFloat()
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return value
}
/**
 * String转double
 * @param str
 * @return
 */
fun toDouble(str: String?): Double {
    if (str.isNullOrBlank()) return 0.0
    var value = 0.0
    try {
        value = str.toDouble()
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return value
}

/**
 * String转Int
 * @param str
 * @return
 */
fun toInt(str: String?): Int {
    if (str == null) return 0
    var value = 0
    try {
        value = str.toInt()
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return value
}


/**
 * String转Int
 * @param str
 * @return
 */
fun toLong(str: String?): Long {
    if (str == null) return 0
    var value = 0L
    try {
        value = str.toLong()
    } catch (e: Exception) {
        value = 0L
        e.printStackTrace()
    }
    return value
}


/**
 * 获取手机唯一标识序列号
 *
 * @return 手机唯一标识序列号
 */
fun getUniqueSerialNumber(): String {
    val phoneName = Build.MODEL
    val manuFacturer = Build.MANUFACTURER
    return "$manuFacturer-$phoneName"
}

/**
 * TextView 设置drawable扩展函数
 * @param drawable
 */
fun TextView.setDrawable(drawableRes: Int?, position: String = "left") {
    var drawable:Drawable? = null
    if (drawableRes != null){
        drawable = gDrawable(drawableRes)
    }
    // 这一步必须要做，否则不会显示。
    drawable?.setBounds(0, 0, drawable.getMinimumWidth(),
            drawable.getMinimumHeight())
    when (position) {
        "left" -> setCompoundDrawables(drawable, null, null, null)
        "top" -> setCompoundDrawables(null, drawable, null, null)
        "right" -> setCompoundDrawables(null, null, drawable, null)
        "bottom" -> setCompoundDrawables(null, null, null, drawable)
    }

}

/**
 * 复制功能
 * @param context 上下文
 * @param content 需要复制的内容
 */
fun copy(context: Context, content: String,tips:String = "复制成功") {
    val systemService = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
    val mClipData = ClipData.newPlainText("Simple text", content);
    //把clip对象放在剪贴板中
    systemService.setPrimaryClip(mClipData)
    if (!tips.isEmpty()){
        T.instance.s(tips)
    }

}

/**
 * 手机号正则验证
 * @param phone 手机号
 * @return boolean
 */
fun isPhone(phone: String): Boolean {
    if (Pattern.matches("^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\\d{8}$", phone)) {
        return true
    } else {
        return false
    }
}

/**
 * 对字符串进行MD5加密
 * @param str
 * @return String
 */
fun getMD5(str: String): String? {
    return try {
        val md = MessageDigest.getInstance("MD5")
        md.update(str.toByteArray())
        BigInteger(1, md.digest()).toString(16)
    } catch (e: java.lang.Exception) {
        throw RuntimeException("MD5加密出现错误")
    }
}

/**
 * 开启缩放渐变呼吸动画
 */
fun startScaleBreathAnimation(tvShowBreathing: ImageView) {
    val anim = ScaleAnimation(
        1.0f,
        1.1f,
        1.0f,
        1.1f,
        Animation.RELATIVE_TO_SELF,
        0.5f,
        Animation.RELATIVE_TO_SELF,
        0.5f
    )
    anim.duration = 750
    anim.repeatMode = Animation.REVERSE // 放大并缩小，时间为750*2
    anim.repeatCount = 1 // 无限循环
    tvShowBreathing.animation = anim
    tvShowBreathing.startAnimation(tvShowBreathing.animation)
}


/**
 * 读取json文件
 * @param context 上下文
 * @param fileName 文件名称
 */

fun getJson(context:Context,fileName:String):String{
    val assetManager: AssetManager = context.getAssets() //获得assets资源管理器（assets中的文件无法直接访问，可以使用AssetManager访问）
    val inputStreamReader = InputStreamReader(assetManager.open(fileName), "UTF-8") //使用IO流读取json文件内容
    val br = BufferedReader(inputStreamReader) //使用字符高效流
    var line: String?
    val builder = java.lang.StringBuilder()
    while (br.readLine().also { line = it } != null) {
        builder.append(line)
    }
    br.close()
    inputStreamReader.close()
    return builder.toString()
}




/**
 *
 * 开启补间动画
 * @param context 上下文
 * @param animation 动画资源
 */
fun View.mStartAnimation(context: Context,animation:Int){
    startAnimation(
        AnimationUtils.loadAnimation(
            context,
            animation
        )
    )
}

/**
 * 开启属性动画
 */
fun View.alphaAnimator(){
    val animator = ObjectAnimator.ofFloat(this, "alpha", 0f, 1f)
    animator.duration = 1000
    animator.start()
}


/**
 * 是否是回看视频
 * @param videoType 视频类型
 */
fun isLookBackVideo(videoType:Int?):Boolean{
    if (videoType == 2 || videoType == 3 || videoType == 4){
        return true
    }
    return false
}

/**
 * 加载圆形图片
 * @param context 上下文
 * @param url 图片地址
 * @param imgView 组件
 */
fun getRoundedCornerBitmap(context: Context,url: String,imgView :ImageView) {
//    val options = RequestOptions.circleCropTransform()
//    Glide.with(context).asBitmap()
//        .load(url)
//        .apply(options)
//        .into(imgView)
}


private var exittime: Long = 0

fun mStartActivity(activity: Activity, cls: Class<*>, extras: Bundle? = null,isFinish:Boolean = false) {
    if (System.currentTimeMillis() - exittime > 500) {
        exittime = System.currentTimeMillis()
        val intent = Intent(activity, cls)
        if (extras != null) {
            intent.putExtras(extras)
        }
        activity.startActivity(intent)
        if (isFinish){
            activity.finish()
        }
    }
}

fun parseDateTime(inputTime:String):String{

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        val outputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

        val date = inputFormat.parse(inputTime)
        val outputTime: String = outputFormat.format(date)
        return outputTime
    } else {
        return ""
    }


}

fun formatSize(size: Long): String {
    val units = arrayOf("B", "KB", "MB", "GB")
    var resultSize = size.toDouble()
    var index = 0
    while (resultSize > 1024 && index < units.size - 1) {
        resultSize /= 1024
        index++
    }
    return String.format("%.2f %s", resultSize, units[index])
}


/**
 * ipv4正则验证
 * @param ip
 */
fun isIPV4(ip:String):Boolean{
    val ipv4Pattern = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$"

    val pattern = Pattern.compile(ipv4Pattern)
    val matcher = pattern.matcher(ip)

    if (matcher.matches()) {
        return true
    } else {
        return false
    }
}


/**
 * 将浮点数精确到后两位
 *
 * @param value 原始浮点数
 * @return 精确到后两位的浮点数
 */
fun roundToTwoDecimalPlaces(value: Float): Float {
    val bigDecimal = BigDecimal(value.toString())
    return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).toFloat()
}

// 获取设备电量信息的方法
fun getBatteryLevel(context: Context): Int {
    val intentFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
    val batteryStatus: Intent? = context.registerReceiver(null, intentFilter)

    val level: Int = batteryStatus?.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) ?: -1
    val scale: Int = batteryStatus?.getIntExtra(BatteryManager.EXTRA_SCALE, -1) ?: -1

    val batteryPct: Float = level / scale.toFloat()
    return (batteryPct * 100).toInt()
}

/**
 * 米每秒转换成海里每小时
 * @param metersPerSecond 米每秒
 */
fun convertMetersPerSecondToNauticalMilesPerHour(metersPerSecond: Float): Float {
    if (metersPerSecond == 0f){
        return 0f
    }
    val metersPerHour = metersPerSecond * 3600 // 转换为米每小时
    val nauticalMiles = metersPerHour / 1852 // 转换为海里
    if (nauticalMiles.isNaN()){
        return 0f
    }else{
        return roundToTwoDecimalPlaces(nauticalMiles)
    }
}

fun View.ScaleSmall(){
    val scaleXAnimator = ObjectAnimator.ofFloat(this, "scaleX", 1f, 0.5f)
    scaleXAnimator.duration = 700
    val scaleYAnimator = ObjectAnimator.ofFloat(this, "scaleY", 1f, 0.5f)
    scaleYAnimator.duration = 700
    val animatorSet = AnimatorSet()
    animatorSet.playTogether(scaleXAnimator, scaleYAnimator)
    animatorSet.start()
}

fun View.ScaleBig(){
    val scaleXAnimator = ObjectAnimator.ofFloat(this, "scaleX", 0.5f, 1f)
    scaleXAnimator.duration = 700
    val scaleYAnimator = ObjectAnimator.ofFloat(this, "scaleY", 0.5f, 1f)
    scaleYAnimator.duration = 700
    val animatorSet = AnimatorSet()
    animatorSet.playTogether(scaleXAnimator, scaleYAnimator)
    animatorSet.start()
}

/**
 * 获取手机号
 */
fun getPhoneNumber(context: Context): String {
    val telephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    if (telephonyManager.line1Number == null)
        return ""
    return telephonyManager.line1Number
}

/**
 * 验证手机号
 * @param phoneNumber
 */
fun validatePhoneNumber(phoneNumber: String): Boolean {
    val regex = Regex("^1[3456789]\\d{9}$")
    return regex.matches(phoneNumber)
}

/**
 * 校验验证码
 * @param input
 */
fun validateFourDigitNumber(input: String): Boolean {
    val regex = Regex("^\\d{4}$")
    return regex.matches(input)
}

fun ImageView.loadImage(url: String) {
    GlideUtils.loadImage(url, this)
}

fun ImageView.loadImage(url: Int) {
    GlideUtils.loadImage(url, this)
}
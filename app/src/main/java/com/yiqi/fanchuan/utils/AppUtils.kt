package com.yiqi.fanchuan.utils

import android.widget.RadioButton
import com.yiqi.fanchuan.R

/**
 * @Author zyf
 * @Date 2023/9/19 14:58
 * @Describe
 */

/**
 * 获取比赛状态
 * @param state
 */
fun getCompetitionState(state:Int):String{
    return when(state){
        0->"未开始"
        1->"进行中"
        else->"已结束"
    }
}

/**
 * 获取比赛状态
 * @param state
 */
fun getCompetitionColor(state:Int):Int{
    return when(state){
        1->R.color.col_27F658
        else->R.color.col_B9B6B6
    }
}

/**
 * 获取用户在线状态
 * @param state
 */
fun getOnlineState(state:Int):Int{
    return when(state){
        0-> R.drawable.offline_bg
        else->R.drawable.online_bg
    }
}

/**
 * 修改选中状态
 * @param tab 按钮
 * @param textSize 文字大小
 * @param typeface 文字样式
 */
fun setTabCheckedState(tab: RadioButton, textSize: Float, typeface: Int) {
    tab.textSize = textSize
    tab.setTypeface(null, typeface)
}
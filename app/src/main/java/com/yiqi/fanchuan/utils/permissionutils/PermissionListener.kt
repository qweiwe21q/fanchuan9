package com.capacity.light.util.permissionutils

/**
 * Date: 2020-2-28
 * Time: 10:49
 * email: 1820458320@qq.com
 */
interface PermissionListener{
    fun onSucceed()

    fun onFiled()
}
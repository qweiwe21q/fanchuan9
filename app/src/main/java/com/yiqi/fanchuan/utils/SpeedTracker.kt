package com.yiqi.fanchuan.utils

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.Log

class SpeedTracker(context: Context) : SensorEventListener {
    private val sensorManager: SensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    private val accelerometer: Sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
    private val gyroscope: Sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
    private var lastAccelerometerData: FloatArray = FloatArray(3)
    private var lastGyroscopeData: FloatArray = FloatArray(3)
    private var lastTimestamp: Long = 0
    fun startTracking() {
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL)
        sensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_NORMAL)
    }
    fun stopTracking() {
        sensorManager.unregisterListener(this)
    }
    override fun onSensorChanged(event: SensorEvent) {
        when (event.sensor.type) {
            Sensor.TYPE_ACCELEROMETER -> lastAccelerometerData = event.values.clone()
            Sensor.TYPE_GYROSCOPE -> lastGyroscopeData = event.values.clone()
        }
        if (lastTimestamp != 0L) {
            val deltaAccelerometer = FloatArray(3)
            val deltaGyroscope = FloatArray(3)
            val timeElapsed = event.timestamp - lastTimestamp
            // 计算加速度变化
            deltaAccelerometer[0] = (lastAccelerometerData[0] - event.values[0]) * timeElapsed * timeElapsed
            deltaAccelerometer[1] = (lastAccelerometerData[1] - event.values[1]) * timeElapsed * timeElapsed
            deltaAccelerometer[2] = (lastAccelerometerData[2] - event.values[2]) * timeElapsed * timeElapsed
            // 计算陀螺仪变化
            deltaGyroscope[0] = (lastGyroscopeData[0] - event.values[0]) * timeElapsed
            deltaGyroscope[1] = (lastGyroscopeData[1] - event.values[1]) * timeElapsed
            deltaGyroscope[2] = (lastGyroscopeData[2] - event.values[2]) * timeElapsed
            // 计算速度
            val speed = Math.sqrt(
                (deltaAccelerometer[0] * deltaAccelerometer[0] +
                        deltaAccelerometer[1] * deltaAccelerometer[1] +
                        deltaAccelerometer[2] * deltaAccelerometer[2]).toDouble()
            )
            // 这里的speed是一个相对的速度，需要根据具体场景和需求进行转换和调整
            // 在这里可以将速度数据传递给其他模块或进行相应的处理
            Log.d("zyf speed:","speed:${speed}")
        }
        lastTimestamp = event.timestamp
    }
    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
        // 精度变化时的处理
    }
}
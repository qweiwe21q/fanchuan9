package com.capacity.light.util.permissionutils

import android.content.Context
import android.net.ConnectivityManager

/**
 * Date: 2020-3-6
 * Time: 10:07
 * email: 1820458320@qq.com
 */
fun isNetConnected(context: Context): Boolean {
    val isNetConnected: Boolean
    // 获得网络连接服务
    val connManager = context
        .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val info = connManager.activeNetworkInfo
    if (info != null && info.isAvailable) {
        isNetConnected = true
    } else {
        isNetConnected = false
    }
    return isNetConnected && isNetConnected
}
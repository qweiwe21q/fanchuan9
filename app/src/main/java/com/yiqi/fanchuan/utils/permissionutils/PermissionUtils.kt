package com.capacity.light.util.permissionutils

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat
import java.util.ArrayList

/**
 * Date: 2020-2-28
 * Time: 10:57
 * email: 1820458320@qq.com
 */
object PermissionUtils {

    //判断版本是否是6.0
    fun isVersionCodeM():Boolean{
        return Build.VERSION.SDK_INT>=Build.VERSION_CODES.M
    }

    /**
     *
     * @param context
     * @param permissions
     * @return 获取未授权的集合
     */
    fun getDeniedList(context: Context, vararg permission:String ): List<String> {
        //创建一个未授权的集合
        val deniedList = ArrayList<String>()
        //如果传入为null,返回空的集合
        if (permission.size == 0) {
            return deniedList
        }
        for (permission in permission) {
            val permissionCode = ContextCompat.checkSelfPermission(context, permission)
            if (permissionCode == PackageManager.PERMISSION_DENIED) {
                //将没有授权的添加到集合
                deniedList.add(permission)
            }
        }
        return deniedList
    }

    fun getDenied(context: Context, permission:Array<out String> ): List<String> {
        //创建一个未授权的集合
        val deniedList = ArrayList<String>()
        //如果传入为null,返回空的集合
        if (permission.size == 0) {
            return deniedList
        }
        for (permission in permission) {
            val permissionCode = ContextCompat.checkSelfPermission(context, permission)
            if (permissionCode == PackageManager.PERMISSION_DENIED) {
                //将没有授权的添加到集合
                deniedList.add(permission)
            }
        }
        return deniedList
    }

}
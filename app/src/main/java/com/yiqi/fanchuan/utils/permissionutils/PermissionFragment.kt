package com.capacity.light.util.permissionutils

import android.Manifest
import android.app.Activity
import android.app.Fragment
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.yiqi.fanchuan.R

/**
 * Date: 2020-2-28
 * Time: 10:47
 * email: 1820458320@qq.com
 */
class PermissionFragment : Fragment() {

    val REQUESTCODE: Int = 66
    val REQUEST_PERMISSION_SETTING: Int = 55
    private lateinit var mPermissionListener: PermissionListener
    private lateinit var mActivity: Activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true

    }

    fun setPermissionListener(permissionListener: PermissionListener) {
        this.mPermissionListener = permissionListener
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun requestPermissions(activity: Activity, permission: Array<String>) {
        this.mActivity = activity
        requestPermissions(permission, REQUESTCODE)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUESTCODE) {
            // 获取未申请的权限列表
            var deniedPermissions = PermissionUtils.getDenied(mActivity, permissions)
            if (deniedPermissions.size > 0) {
                // 执行失败的方法
                onFailed(permissions)
            } else {
                // 执行成功的方法
                onSucceed()
            }
        }
    }

    /**
     * 成功时回调方法
     */
    fun onSucceed() {
        if (mPermissionListener != null) {
            mPermissionListener.onSucceed()
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun onFailed(permission: Array<out String>) {
        if (permission.size > 1 && shouldShowRequestPermissionRationale(permission[0])) {
            mPermissionListener.onFiled()
            return
        }
        for(item in permission){
            if (!shouldShowRequestPermissionRationale(item)) {
                AlertDialog.Builder(mActivity)
                    .setTitle(R.string.permission_denied)
                    .setMessage(resources.getString(R.string.open_permissions) + getPermissionName(item) + resources.getString(R.string.permissions))
                    .setPositiveButton(resources.getString(R.string.go_to_open)) { dialog, which ->
                        dialog.dismiss()
                        openSetting()
                    }
                    .setNegativeButton(
                        resources.getString(R.string.cancel)
                    ) { dialog, which ->
                        dialog.dismiss()
                        mPermissionListener.onFiled()
                    }
                    .show()
                return
            }
        }
//        for (index in 0..permission.size-1) {
//            if (!shouldShowRequestPermissionRationale(permission[index])) {
//                AlertDialog.Builder(mActivity)
//                    .setTitle(R.string.permission_denied)
//                    .setMessage(resources.getString(R.string.open_permissions) + getPermissionName(permission[index]) + resources.getString(R.string.permissions))
//                    .setPositiveButton(resources.getString(R.string.go_to_open)) { dialog, which ->
//                        dialog.dismiss()
//                        openSetting()
//                    }
//                    .setNegativeButton(
//                        resources.getString(R.string.cancel)
//                    ) { dialog, which ->
//                        dialog.dismiss()
//                        mPermissionListener.onFiled()
//                    }
//                    .show()
//                return
//            }
//        }
        mPermissionListener.onFiled()
    }


    /**
     * 打开应用设置页面
     */
    fun openSetting(){
        var intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        var uri = Uri.fromParts("package",mActivity.packageName,null)
        intent.setData(uri)
        startActivityForResult(intent,REQUEST_PERMISSION_SETTING)
    }



    fun getPermissionName(permission:String):String{
        when(permission){
            Manifest.permission.CAMERA->{
                return resources.getString(R.string.camera)
            }
            Manifest.permission.WRITE_EXTERNAL_STORAGE->{
                return resources.getString(R.string.read_and_write_phone_storage)
            }
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION->{
                return resources.getString(R.string.position_information)
            }
        }
        return ""
    }


}





























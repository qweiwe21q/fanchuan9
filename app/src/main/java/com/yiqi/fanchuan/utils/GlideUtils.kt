package com.yiqi.bianjiema.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.MotionEvent
import android.view.RoundedCorner
import android.widget.ImageView
import androidx.compose.animation.core.Transition
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.yiqi.fanchuan.R
import java.lang.ref.WeakReference


/**
 * @Author zyf
 * @Date 2023/2/13 09:51
 * @Describe
 */

object GlideUtils {

    /*** 占位图  */
    var placeholderImage = R.mipmap.ic_launcher

    /*** 错误图  */
    var errorImage = R.mipmap.ic_launcher

    private val options by lazy {  RequestOptions()
        .placeholder(placeholderImage) //占位图
        .error(errorImage) //错误图
         }

    /**
     * 加载图片(默认)
     *
     * @param context   上下文
     * @param url       链接
     * @param imageView ImageView
     */
    fun loadImage( url: String?, imageView: ImageView) {
        Glide.with(imageView.context).load(url).into(imageView)
    }

    fun loadImage( url: Int, imageView: ImageView) {
        Glide.with(imageView.context).load(url).into(imageView)
    }

    fun loadImageRound( url: Int, imageView: ImageView,roundingRadius:Int = 20) {
        Glide.with(imageView.context).load(url).apply(RequestOptions.bitmapTransform(RoundedCorners(roundingRadius))) .into(imageView)
    }

    fun loadImageRound( url: String?, imageView: ImageView,roundingRadius:Int = 20) {
        Glide.with(imageView.context).load(url).apply(RequestOptions.bitmapTransform(RoundedCorners(roundingRadius))) .into(imageView)
    }

    fun loadCircularImage( imageUrl: String, imageView: ImageView) {
        Glide.with(imageView.context)
            .load(imageUrl)
            .apply(RequestOptions.circleCropTransform())
            .into(imageView)
    }

    fun loadCircularImage( imageUrl: Int, imageView: ImageView) {
        Glide.with(imageView.context)
            .load(imageUrl)
            .apply(RequestOptions.circleCropTransform())
            .into(imageView)
    }

    fun loadImagePressed(imageView: ImageView, normalImage: Int, pressedImage: Int) {
        val requestManager = Glide.with(imageView.context)
//        val normalRequest = requestManager.load(normalImage).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.color.transparent).dontAnimate()
//        val pressedRequest = requestManager.load(pressedImage).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.color.transparent).dontAnimate()
     val normalRequest = requestManager.load(normalImage).placeholder(R.color.transparent).dontAnimate()
        val pressedRequest = requestManager.load(pressedImage).placeholder(R.color.transparent).dontAnimate()

        // 设置 ImageView 的初始状态为正常状态
        normalRequest.into(imageView)
        // 监听 ImageView 的按下和抬起事件，根据事件切换图片状态
        imageView.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // 切换到按下状态的图片

                    pressedRequest.into(imageView)
                }
                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                    // 切换到正常状态的图片
                    normalRequest.into(imageView)
//                    Glide.with(imageView.context).load(normalImage).dontTransform().dontAnimate().diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .into(imageView)

                }
            }
            false
        }
    }
    fun  cleanCache(context: Context){
        Glide.get(context).clearMemory()
//        Glide.get(context).clearDiskCache();
    }
}
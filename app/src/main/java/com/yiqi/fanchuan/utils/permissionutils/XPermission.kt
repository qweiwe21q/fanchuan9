package com.capacity.light.util.permissionutils

import android.app.Activity
import android.os.Build
import androidx.annotation.RequiresApi
import java.util.*

/**
 * Date: 2020-2-28
 * Time: 13:11
 * email: 1820458320@qq.com
 */
class XPermission constructor(activity: Activity){
    private var mActivity: Activity
    private var mPermissionFragment:PermissionFragment
    private val TAG :String = "XPermission"
    private var mPermission= arrayOf<String>()

    init {
        this.mActivity = activity
        mPermissionFragment = getPermissionFragment(mActivity)
    }

    fun getPermissionFragment(activity:Activity):PermissionFragment{
        var permissionFragment = findPermissionFragment(activity)
        var isNewInstance = (permissionFragment==null)
        if (isNewInstance){
            permissionFragment = PermissionFragment()
            var fragmentManager = activity.fragmentManager
            fragmentManager.beginTransaction()
                .add(permissionFragment,TAG)
                .commitAllowingStateLoss();
            fragmentManager.executePendingTransactions()
        }
        return permissionFragment!!;
    }

    fun findPermissionFragment(activity:Activity):PermissionFragment?{
        return  activity.fragmentManager.findFragmentByTag(TAG) as? PermissionFragment
    }

    fun permissions( permissions: Array<String>): XPermission {
        mPermission = permissions
        return this
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun request(permissionListener:PermissionListener){
        mPermissionFragment.setPermissionListener(permissionListener)
        // 1.判断版本
        if (!PermissionUtils.isVersionCodeM()){
            // 版本6.0一下，直接回调成功方法
            permissionListener.onSucceed()
            return
        }
        // 版本6.0及以上
        // 2.获取未授权的列表
        var deniedList = PermissionUtils.getDenied(mActivity,mPermission)
        if (deniedList.size>0){
            // 3.去申请权限
            mPermissionFragment.requestPermissions(mActivity,deniedList.toTypedArray())
        }else{
            permissionListener.onSucceed()
        }
    }


}
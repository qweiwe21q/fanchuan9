package com.yiqi.fanchuan.utils

object PhoneUtils {
    /**
     * 将手机号中间4位替换为星号
     */
    fun maskPhoneNumber(phoneNumber: String): String {
        val regex = "(\\d{3})\\d{4}(\\d{4})".toRegex()
        return phoneNumber.replace(regex, "$1****$2")
    }
}
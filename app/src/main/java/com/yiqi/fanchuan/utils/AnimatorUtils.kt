package com.yiqi.fanchuan.utils

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.view.View

/**
 * @Author zyf
 * @Date 2023/10/16 14:12
 * @Describe
 */

object AnimatorUtils {

    fun View.rotate(degrees: Float, duration: Long = 1000L) {

        val animator = ObjectAnimator.ofFloat(this, "rotation", 0f, degrees)
        animator.duration = duration

        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                // 动画结束回调
            }
        })

        animator.start()

    }
}
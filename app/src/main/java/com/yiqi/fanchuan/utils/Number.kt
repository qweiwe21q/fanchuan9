package com.yiqi.fanchuan.utils.log

/**
 * @Author zyf
 * @Date 2023/8/29 14:30
 * @Describe
 */

/**
 * 省略小数点
 */
fun Double.truncateDecimal( decimalPlaces: Int): Double {
    val format = "%.${decimalPlaces}f"
    return String.format(format, this).toDouble()
}

/**
 * 省略小数点
 */
fun Float.truncateDecimal( decimalPlaces: Int): Double {
    val format = "%.${decimalPlaces}f"
    return String.format(format, this).toDouble()
}
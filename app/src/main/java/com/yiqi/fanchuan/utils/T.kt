package com.yiqi.fanchuan.utils

import android.content.Context
import android.os.Looper
import android.text.TextUtils
import android.widget.Toast
import com.yiqi.fanchuan.MyApp
import com.yiqi.fanchuan.widget.CustomToast

/**
 * Author: zyf
 * Date: 2021/12/22
 * Time: 17:38
 */
class T private constructor(){

    private object SingletonHolder {
        val holder= T()
    }

    var mToast: Toast? = null
    companion object{
        val instance = SingletonHolder.holder
        var app: MyApp? = null
    }

    fun init(app: MyApp) {
        Companion.app = app
    }

    fun s(msg: String) {
        if (app == null || TextUtils.isEmpty(msg)) return
        s(app!!, msg)
    }

    fun s(resId: Int) {
        if (app == null) return
        s(app!!, resId)
    }

    fun l(msg: String) {
        if (app == null || TextUtils.isEmpty(msg)) return
        l(app!!, msg)
    }

    fun l(resId: Int) {
        if (app == null) return
        l(app!!, resId)
    }


    fun s(context: Context, msg: String) {
        try {
            showToast(context,msg, Toast.LENGTH_SHORT)
        } catch (e: Exception) {
            Looper.prepare()
            showToast(context,msg, Toast.LENGTH_SHORT)
            Looper.loop()
        }

    }

    fun s(context: Context, msg: Int) {
        s(context,context.resources.getString(msg))
    }

    fun l(context: Context, msg: String) {
        try {
            showToast(context,msg, Toast.LENGTH_LONG)
        } catch (e: Exception) {
            Looper.prepare()
            showToast(context,msg, Toast.LENGTH_LONG)
            Looper.loop()
        }
    }

    fun l(context: Context, msg: Int) {
        l(context,context.resources.getString(msg))
    }

    fun showToast(context: Context, msg:String, duration:Int){
        if (mToast!=null){
            mToast?.cancel()
            mToast= CustomToast.makeText(context,msg,duration)
        }else{
            mToast = CustomToast.makeText(context, msg, duration)
        }
        mToast?.show()
    }
}
package com.yiqi.fanchuan.ui.competition

import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.listener.OnItemClickListener
import com.yiqi.fanchuan.adapter.CompetitionAdapter
import com.yiqi.fanchuan.base.activity.BaseActivity
import com.yiqi.fanchuan.bean.CompetitionBean
import com.yiqi.fanchuan.databinding.ActivityCompetitionBinding
import com.yiqi.fanchuan.utils.mStartActivity
import com.yiqi.fanchuan.utils.setOnClickListener
import com.yiqi.fanchuan.utils.setTabCheckedState
import com.yiqi.fanchuan.widget.ItemSpacingDecoration

/**
 * @Author zyf
 * @Date 2023/8/25 10:38
 * @Describe 比赛列表
 */

class CompetitionActivity :BaseActivity<ActivityCompetitionBinding>(){

    private val mData by lazy { mutableListOf(
        CompetitionBean("2023/12/12 12:00","天津跳水比赛",0),
        CompetitionBean("2023/12/12 12:00","天津跳水比赛",1),
        CompetitionBean("2023/12/12 12:00","天津跳水比赛",2),
    ) }

    private val mCompetitionAdapter by lazy { CompetitionAdapter() }

    override fun initView() {
        viewBinding.rvCompetition.apply {
            adapter = mCompetitionAdapter
            layoutManager = LinearLayoutManager(mContext)
            addItemDecoration(ItemSpacingDecoration(10))
        }
    }

    override fun initClick() {
        setOnClickListener(viewBinding.title.getBackView()){
            when(this){
                viewBinding.title.getBackView()->{
                    finish()
                }
            }
        }

        viewBinding.rgTab.setOnCheckedChangeListener { group, checkedId ->
            when(checkedId){
                viewBinding.rbNotStarted.id -> {
                    setTabCheckedState(viewBinding.rbNotStarted, 22f, Typeface.BOLD)
                    setTabCheckedState(viewBinding.rbInYiprogress, 20f, Typeface.NORMAL)
                    setTabCheckedState(viewBinding.rbEnded, 20f, Typeface.NORMAL)
                }
                viewBinding.rbInYiprogress.id -> {
                    setTabCheckedState(viewBinding.rbInYiprogress, 22f, Typeface.BOLD)
                    setTabCheckedState(viewBinding.rbNotStarted, 20f, Typeface.NORMAL)
                    setTabCheckedState(viewBinding.rbEnded, 20f, Typeface.NORMAL)
                }
                viewBinding.rbEnded.id -> {
                    setTabCheckedState(viewBinding.rbEnded, 22f, Typeface.BOLD)
                    setTabCheckedState(viewBinding.rbNotStarted, 20f, Typeface.NORMAL)
                    setTabCheckedState(viewBinding.rbInYiprogress, 20f, Typeface.NORMAL)
                }
            }
        }
    }

    override fun initData() {
        mCompetitionAdapter.setNewInstance(mData)
    }
}
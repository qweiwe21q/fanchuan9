package com.yiqi.dangjian.constants

import com.yiqi.bianjiema.utils.SPUtils
import com.yiqi.fanchuan.bean.UserInfoBean

/**
 * @Author zyf
 * @Date 2023/7/25 11:19
 * @Describe
 */

object Constants {

    const val JSON_PARSE = "application/json; charset=utf-8"


    const val SP_NICK_NAME = "sp_nick_name"  //姓名

    const val SP_UCODE = "sp_ucode"   //用户 id

    const val SP_CLUB_NAME = "sp_club_name"  //俱乐部名称

    const val SP_GROUP_ID = "sp_group_id"  //俱乐部 id

    const val SP_LEVEL = "sp_level"  //俱乐部等级

    const val TYPE_PULSE_MONITORING = "TYPE_PULSE_MONITORING" //心率检测

    const val TYPE_SPO2_MEASUREMENT = "type_spo2_measurement" //血氧检测

    /**
     * 保存用户信息
     */
    fun saveUserInfo(userInfo:UserInfoBean){
        userInfo.apply {
            SPUtils.saveValue(SP_NICK_NAME,nickname)
            SPUtils.saveValue(SP_UCODE,ucode)
            SPUtils.saveValue(SP_CLUB_NAME,club)
            SPUtils.saveValue(SP_GROUP_ID,groupid)
            SPUtils.saveValue(SP_LEVEL,level)
        }
    }

    /**
     * 清除用户信息
     */
    fun cleanUserInfo(){
        SPUtils.apply {
            saveValue(SP_NICK_NAME,"")
            saveValue(SP_UCODE,"")
            saveValue(SP_CLUB_NAME,"")
            saveValue(SP_GROUP_ID,"")
            saveValue(SP_LEVEL,"")
        }
    }

}
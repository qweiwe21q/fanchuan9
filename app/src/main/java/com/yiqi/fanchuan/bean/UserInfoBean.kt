package com.yiqi.fanchuan.bean

/**
 * @Author zyf
 * @Date 2023/9/1 14:52
 * @Describe
 */

data class UserInfoBean(
    val club: String,
    val function: String,
    val groupid: String,
    val headicon: String,
    val id: Int,
    val isonline: Int,
    val level: String,
    val movearraw: Int,
    val nickname: String,
    val pathlist: String,
    val power: String,
    val racetype: Int,
    val shiptype: Int,
    val shipyear: String,
    val showNumber: String,
    val speed: Int,
    val ucode: String,
    val x: String,
    val y: String
)
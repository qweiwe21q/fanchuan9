package com.yiqi.fanchuan.bean

/**
 * @Author zyf
 * @Date 2023/9/19 17:31
 * @Describe
 */

data class ContactBean(val avatar:String,val name:String,val onlineState:Int)
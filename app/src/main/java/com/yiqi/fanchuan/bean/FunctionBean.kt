package com.yiqi.fanchuan.bean

/**
 * @Author zyf
 * @Date 2023/10/12 10:54
 * @Describe
 */

data class FunctionBean(var name:String ,var icon:Int)
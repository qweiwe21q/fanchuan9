package com.yiqi.fanchuan.bean

/**
 * @Author zyf
 * @Date 2023/9/19 13:41
 * @Describe
 */

data class CompetitionBean (
    val time:String,
    val name:String,
    val state:Int  //0 未开始  1 进行中 2 已结束
)
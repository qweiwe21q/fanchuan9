package com.yiqi.fanchuan.bean

/**
 * @Author zyf
 * @Date 2023/9/1 15:55
 * @Describe
 */

data class WeatherBean(
    val humidity: String,
    val infotime: String,
    val oceandirection: String,
    val oceanpower: String,
    val temperature: String,
    val winddirection: String,
    val windpower: String
)
package com.yiqi.fanchuan.adapter

import android.widget.ImageView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.yiqi.fanchuan.R
import com.yiqi.fanchuan.bean.CompetitionBean
import com.yiqi.fanchuan.utils.getCompetitionColor
import com.yiqi.fanchuan.utils.getCompetitionState
import com.yiqi.fanchuan.utils.loadImage

/**
 * @Author zyf
 * @Date 2023/9/19 11:42
 * @Describe
 */

class CompetitionAdapter :BaseQuickAdapter<CompetitionBean,BaseViewHolder>(R.layout.item_competition){

    override fun convert(holder: BaseViewHolder, item: CompetitionBean) {
        holder.setText(R.id.tv_competition_name,item.name)
            .setText(R.id.tv_competition_time,item.time)
            .setText(R.id.tv_competition_state, getCompetitionState(item.state))
            .setTextColorRes(R.id.tv_competition_state,getCompetitionColor(item.state))
        val ivState = holder.getView<ImageView>(R.id.iv_state)
        ivState.loadImage(if (item.state == 1) R.mipmap.icon_in_progress else R.drawable.gray_round)
    }
}
package com.yiqi.fanchuan.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.yiqi.fanchuan.R
import com.yiqi.fanchuan.bean.ContactBean
import com.yiqi.fanchuan.utils.getOnlineState

/**
 * @Author zyf
 * @Date 2023/9/19 16:57
 * @Describe
 */

class ContactAdapter :BaseQuickAdapter<ContactBean,BaseViewHolder>(R.layout.item_contact){

    override fun convert(holder: BaseViewHolder, item: ContactBean) {
        holder.setText(R.id.tv_name,item.name)
            .setImageResource(R.id.iv_online, getOnlineState(item.onlineState))
            .setImageResource(R.id.iv_ng,R.drawable.item_contact_pressed)
    }
}
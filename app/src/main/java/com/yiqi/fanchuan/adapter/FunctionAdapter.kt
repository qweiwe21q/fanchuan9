package com.yiqi.fanchuan.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.yiqi.fanchuan.R

/**
 * @Author zyf
 * @Date 2023/7/10 17:02
 * @Describe
 */

class FunctionAdapter(data:MutableList<String>) :BaseQuickAdapter<String,BaseViewHolder>(R.layout.item_function,data){

    override fun convert(holder: BaseViewHolder, item: String) {
        holder.setText(R.id.tv_function,item)
    }
}
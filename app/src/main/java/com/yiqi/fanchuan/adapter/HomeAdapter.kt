package com.yiqi.fanchuan.adapter

import android.widget.ImageView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.yiqi.bianjiema.utils.GlideUtils
import com.yiqi.fanchuan.R
import com.yiqi.fanchuan.bean.FunctionBean
import com.yiqi.fanchuan.utils.loadImage

/**
 * @Author zyf
 * @Date 2023/9/18 15:08
 * @Describe  首页列表适配器
 */

class HomeAdapter(list:MutableList<FunctionBean>) :BaseQuickAdapter<FunctionBean,BaseViewHolder>(R.layout.item_home,list){

    override fun convert(holder: BaseViewHolder, item: FunctionBean) {
        holder.setText(R.id.tv_function,item.name)
            .setImageResource(R.id.iv_home_item_bg,if (holder.layoutPosition == 0) R.drawable.blue_gradient_r20_bg else R.drawable.r20_14192e_bg)

        val ivFunction = holder.getView<ImageView>(R.id.iv_function)
        ivFunction.loadImage(item.icon)
    }
}
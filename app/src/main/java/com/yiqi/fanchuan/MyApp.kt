package com.yiqi.fanchuan

import android.app.Application
import android.content.Context
import com.yiqi.fanchuan.utils.T

/**
 * @Author zyf
 * @Date 2023/7/3 17:14
 * @Describe
 */

class MyApp : Application(){

    companion object{

        lateinit var mContext : Context

        /**
         * 获取全局上下文
         * @return context 上下文
         */
        fun getContext(): Context {
            return mContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        mContext = this
        T.instance.init(this)
    }
}
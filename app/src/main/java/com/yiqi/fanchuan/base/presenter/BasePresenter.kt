package com.gehua.app.base.presenter

/**
 * Author: zyf
 * Date: 2021/12/22
 * Time: 10:05
 */


import com.yiqi.fanchuan.base.IView
import com.yiqi.fanchuan.http.BaseObserver
import com.yiqi.fanchuan.http.HttpResult
import com.yiqi.fanchuan.http.RetrofitClient
import com.yiqi.fanchuan.http.rxSchedulerHelper
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.lang.ref.WeakReference

/**
 * Date: 2020-3-2
 * Time: 11:33
 * email: 1820458320@qq.com
 */
open class BasePresenter<V : IView> : IPresenter<V> {

    private lateinit var viewReference: WeakReference<V>
    var disposable: CompositeDisposable = CompositeDisposable()

    fun <T> addSubscribe(observable: Observable<HttpResult<T>>, baseObserver: BaseObserver<T>) {
        val observer = observable
                .compose(rxSchedulerHelper())
                .subscribeWith(baseObserver)
        disposable.add(observer)
    }

    fun unsubscribe() {
        disposable.dispose()
    }

    fun <D> create(clazz: Class<D>): D {
        return RetrofitClient.get().retrofit.create(clazz)
    }


    override fun attachView(view: V) {
        viewReference = WeakReference(view)
    }

    override fun detachView() {
        viewReference.clear()
        unsubscribe()
    }

    override fun isViewAttached(): Boolean {
        return viewReference.get() != null
    }

    override fun getView(): V? {
        return viewReference.get()
    }

}

package com.yiqi.fanchuan.base

/**
 * Date: 2020-3-2
 * Time: 10:59
 * email: 1820458320@qq.com
 */
interface IView {
    fun toast(t:Int)

    fun toast(t:String)

    fun showLoading()

    fun hintDialog()

    fun executeOnLoadDataError(data:Any?)

    fun httpSucceedList(flag: Int, list: List<*>?, isLastPage: Int)

    fun httpSucceedList(flag: Int, list: List<*>?, isLastPage: Int, total: String?)

    fun httpRequestError(flag: Int, msg: String?)
}

package com.yiqi.fanchuan.base.activity

import android.app.Activity
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.activity.ComponentActivity
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import androidx.wear.activity.ConfirmationActivity
import com.yiqi.fanchuan.utils.AppManager
import com.yiqi.fanchuan.utils.T
import java.lang.ref.WeakReference
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.ParameterizedType

abstract class BaseActivity<B : ViewBinding> : ComponentActivity(){

    protected lateinit var mContext : Context

    protected lateinit var mActivity : Activity

    private var activityWR: WeakReference<Activity>? = null

    protected val TAG by lazy { javaClass.simpleName }

    protected lateinit var viewBinding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);  //旋转180度
        Log.d(TAG, "onCreate: ")
        mContext = this
        mActivity = this
        activityWR = WeakReference(mActivity)
        AppManager.instance?.addActivity(this) //添加到栈中
        val type = javaClass.genericSuperclass as ParameterizedType
        val cls = type.actualTypeArguments[0] as Class<*>
        try {
            val inflate = cls.getDeclaredMethod("inflate", LayoutInflater::class.java)
            viewBinding = inflate.invoke(null, layoutInflater) as B
            setContentView(viewBinding.getRoot())
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }

        // 沉浸式状态栏
        hideStatusBar(this)
        initView()
        initData()
        initClick()
    }

    open fun hideStatusBar(activity: Activity) {
        //完全隐藏导航栏
        val window = activity.window ?: return
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        val lp = window.attributes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            lp.layoutInDisplayCutoutMode =
                WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
        }
        window.attributes = lp

        //隐藏底部导航栏
        val decorView = window.decorView
        val uiOptions = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_IMMERSIVE)
        decorView.setSystemUiVisibility(uiOptions)
    }


    abstract fun initView()

    abstract fun initClick()

    abstract fun initData()

    open fun showToast(msg: String){
        T.instance.s(msg)
    }

    open fun showToast(msg: Int){
        T.instance.s(msg)
    }

    //隐藏键盘
    protected fun hideKeyboard() {
        currentFocus?.apply {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        AppManager.instance?.finishActivity(this) //添加到栈中
    }


}
package com.yiqi.fanchuan.base.activity

import android.app.ProgressDialog
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Bundle
import androidx.viewbinding.ViewBinding
import com.gehua.app.base.presenter.IPresenter
import com.yiqi.fanchuan.R
import com.yiqi.fanchuan.base.IView

abstract class BaseMVPActivity <B : ViewBinding,in V : IView, P : IPresenter<in V>>:BaseActivity<B>(),IView{

    protected lateinit var presenter: P

    protected val mDialog by lazy { ProgressDialog(mContext).apply {
        setMessage(resources?.getString(R.string.loading))
    } }

    abstract fun createPresenter(): P

    override fun onCreate(savedInstanceState: Bundle?) {
        initPresenter()
        super.onCreate(savedInstanceState)
    }

    fun initPresenter() {
        presenter = createPresenter()
        presenter.attachView(this as V)
    }

    override fun initView() {

    }

    override fun initData() {

    }

    override fun initClick() {

    }

    //字体适配解决方案
    override fun onConfigurationChanged(newConfig: Configuration) {
        if (newConfig.fontScale != 1f) //非默认值
            resources
        super.onConfigurationChanged(newConfig)
    }

    override fun getResources(): Resources? {
        val res: Resources = super.getResources()
        if (res.getConfiguration().fontScale != 1f) { //非默认值
            val newConfig = Configuration()
            newConfig.setToDefaults() //设置默认
            res.updateConfiguration(newConfig, res.getDisplayMetrics())
        }
        return res
    }

    override fun toast(t: Int) {
        showToast(t)
    }

    override fun toast(t: String) {
        showToast(t)
    }

    override fun showLoading() {
        mDialog.show()
    }

    override fun hintDialog() {
        mDialog.dismiss()
    }

    override fun executeOnLoadDataError(data:Any?) {

    }

    override fun httpRequestError(flag: Int, msg: String?) {

    }

    override fun httpSucceedList(flag: Int, list: List<*>?, isLastPage: Int) {

    }

    override fun httpSucceedList(flag: Int, list: List<*>?, isLastPage: Int, total: String?) {

    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}
package com.gehua.app.base.presenter

import com.yiqi.fanchuan.base.IView


/**
 * Author: zyf
 * Date: 2021/12/22
 * Time: 10:08
 */
interface IPresenter<V : IView> {

    fun attachView(view: V)

    fun detachView()

    fun isViewAttached(): Boolean

    fun getView(): V?
}
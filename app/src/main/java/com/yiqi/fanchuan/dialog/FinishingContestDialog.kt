package com.yiqi.fanchuan.dialog

import android.content.Context
import android.widget.ImageView
import android.widget.TextView
import com.lxj.xpopup.core.CenterPopupView
import com.yiqi.fanchuan.R
import com.yiqi.fanchuan.bean.ContactBean
import com.yiqi.fanchuan.utils.setOnClickListener

/**
 * @Author zyf
 * @Date 2023/9/20 11:04
 * @Describe 结束比赛
 */

class FinishingContestDialog(context:Context, val mCallBack:CallBack) :CenterPopupView(context){

    private val tvCancel by lazy { findViewById<TextView>(R.id.tv_cancel) }

    private val tvSure by lazy { findViewById<TextView>(R.id.tv_sure) }

    override fun getImplLayoutId() = R.layout.dialog_finishing_contest

    override fun onCreate() {
        super.onCreate()
        initClick()
    }

    fun initClick(){
        setOnClickListener(tvCancel,tvSure){
            when(this){
                tvCancel->{
                    dismiss()
                }
                tvSure->{
                    mCallBack.finishingContest()
                    dismiss()
                }
            }
        }
    }

    interface CallBack{
        fun finishingContest()
    }
}
package com.yiqi.fanchuan.dialog

import android.content.Context
import android.widget.ImageView
import android.widget.TextView
import com.lxj.xpopup.core.CenterPopupView
import com.yiqi.fanchuan.R
import com.yiqi.fanchuan.bean.ContactBean
import com.yiqi.fanchuan.utils.setOnClickListener

/**
 * @Author zyf
 * @Date 2023/9/20 11:04
 * @Describe
 */

class CallDialog(context:Context,val mCallBack:CallBack) :CenterPopupView(context){

    private val ivUserAvatar by lazy { findViewById<ImageView>(R.id.iv_user_avatar) }

    private val tvContent by lazy { findViewById<TextView>(R.id.tv_content) }

    private val tvCancel by lazy { findViewById<TextView>(R.id.tv_cancel) }

    private val tvSure by lazy { findViewById<TextView>(R.id.tv_sure) }

    private var mContactBean: ContactBean? = null

    override fun getImplLayoutId() = R.layout.dialog_call

    override fun onCreate() {
        super.onCreate()
        initClick()
        mContactBean?.let { setUserInfo(it) }
    }

    fun initClick(){
        setOnClickListener(tvCancel,tvSure){
            when(this){
                tvCancel->{
                    dismiss()
                }
                tvSure->{
                    mContactBean?.apply {
                        mCallBack.call(this)
                    }
                    dismiss()
                }
            }
        }
    }

    fun setUserInfo(contactBean: ContactBean){
        mContactBean = contactBean
        if (isCreated){
            tvContent.setText("您确定要呼叫${mContactBean!!.name}吗?")
        }
    }

    interface CallBack{
        fun call(contactBean: ContactBean)
    }
}
package com.yiqi.fanchuan

import android.content.Context
import android.widget.TextView
import com.lxj.xpopup.core.CenterPopupView
import com.yiqi.fanchuan.utils.setOnClickListener
/**
 * @Author zyf
 * @Date 2023/8/21 17:50
 * @Describe
 */

class ConfirmStopPopup(context: Context,val mViewClick : viewClick) :CenterPopupView(context) {

    private val tvCancel by lazy { findViewById<TextView>(R.id.tv_cancel) }

    private val tvSure by lazy { findViewById<TextView>(R.id.tv_sure) }

    override fun getImplLayoutId() = R.layout.popup_confirm_stop

    override fun initPopupContent() {
        super.initPopupContent()
        initClick()
    }

    fun initClick(){
        setOnClickListener(tvCancel,tvSure){
            when(this){
                tvCancel->{
                    mViewClick.cancel()
                    dismiss()
                }
                tvSure->{
                    mViewClick.sure()
                    dismiss()
                }
            }
        }
    }

    interface viewClick{
        fun cancel()

        fun sure()
    }

}
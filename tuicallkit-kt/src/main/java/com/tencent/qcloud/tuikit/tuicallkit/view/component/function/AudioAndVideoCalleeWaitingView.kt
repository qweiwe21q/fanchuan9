package com.tencent.qcloud.tuikit.tuicallkit.view.component.function

import android.content.Context
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.tencent.qcloud.tuikit.tuicallengine.TUICallDefine
import com.tencent.qcloud.tuikit.tuicallengine.impl.base.Observer
import com.github.trtc.R
import com.tencent.qcloud.tuikit.tuicallkit.view.root.BaseCallView
import com.tencent.qcloud.tuikit.tuicallkit.viewmodel.component.function.AudioAndVideoCalleeWaitingViewModel

class AudioAndVideoCalleeWaitingView(context: Context) : BaseCallView(context) {

    private var viewModel = AudioAndVideoCalleeWaitingViewModel()

    private val ivDecline by lazy { findViewById<ImageView>(R.id.iv_decline) }

    private val ivAnswer by lazy { findViewById<ImageView>(R.id.iv_answer) }

    private var mediaTypeObserver = Observer<TUICallDefine.MediaType> {

    }

    init {
        initView()
        addObserver()
    }

    override fun clear() {
        removeObserver()
    }

    private fun addObserver() {
        viewModel.mediaType.observe(mediaTypeObserver)
    }

    private fun removeObserver() {
        viewModel.mediaType.removeObserver(mediaTypeObserver)
    }

    private fun initView() {
        LayoutInflater.from(context).inflate(R.layout.tuicallkit_funcation_view_invited_waiting, this)
        initViewListener()
    }

    private fun initViewListener() {
        ivDecline!!.setOnClickListener { viewModel.reject() }
        ivAnswer!!.setOnClickListener { viewModel.accept() }
    }

}